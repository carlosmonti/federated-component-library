const { dependencies } = require('./package.json');

module.exports = {
  name: 'federatedLibrary',
  exposes: {
    './Button': './src/components/Button',
    './CSSReset' : './src/components/CSSReset'
  },
  filename: 'remoteEntry.js',
  shared: {
    ...dependencies,
    react: {
      singleton: true,
      requiredVersion: dependencies['react'],
    },
    'react-dom': {
      singleton: true,
      requiredVersion: dependencies['react-dom'],
    },
  },
};
