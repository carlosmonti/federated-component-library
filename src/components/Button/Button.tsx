export interface ButtonProps extends React.DetailedHTMLProps<React.ButtonHTMLAttributes<HTMLButtonElement>, HTMLButtonElement>, React.AriaAttributes  {
  children?: React.ReactNode
}

const Button: React.FC<ButtonProps> = ({ children }) => <button>{children}</button>;

export default Button;
