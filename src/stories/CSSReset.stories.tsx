import React from 'react';
import { ComponentStory, ComponentMeta } from '@storybook/react';

import { resetCSS, Reset } from '../components';

export default {
  title: 'Federated Components/CSSReset',
  component: Reset
} as ComponentMeta<typeof Reset>;

const Template: ComponentStory<typeof Reset> = (args) => <Reset {...args} />;

export const CSSReset = Template.bind({});
