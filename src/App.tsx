import styled from 'styled-components';

import { Button } from './components';

const StyledApp = styled.div`

`;

function App() {
  return (
    <StyledApp>
      <h1>Federated Components</h1>
      <section>
        <h2>Button</h2>
        <Button>Some Button</Button>
      </section>
    </StyledApp>
  );
}

export default App;
